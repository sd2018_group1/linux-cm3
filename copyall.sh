ROOT_PART=/media/$USER/rootfs
BOOT_PART=/media/$USER/boot
sudo make -j$(nproc) ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_MOD_PATH=$ROOT_PART modules_install
sudo cp firmware/mchp/* $ROOT_PART/lib/firmware/mchp/
sudo cp arch/arm/boot/zImage $BOOT_PART/kernel7.zImage
sudo cp arch/arm/boot/dts/*.dtb $BOOT_PART/
sudo cp arch/arm/boot/dts/overlays/*.dtb* $BOOT_PART/overlays/
sudo cp arch/arm/boot/dts/overlays/README $BOOT_PART/overlays/
