export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabihf-
export OUT_DIR=../cm3-kernel-out

if [ $1 == '--remake-all' ]
	make -j$(nproc) O=$OUT_DIR clean
	make -j$(nproc) O=$OUT_DIR prepare
	make -j$(nproc) O=$OUT_DIR modules_prepare
fi
make -j$(nproc) O=$OUT_DIR zImage dtbs modules
